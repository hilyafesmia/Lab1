// Chatbox
var chathead = document.getElementsByClassName('chat-head');
var chatbody = document.getElementsByClassName('chat-body');

$(chathead).click(function(){
    $(chatbody).toggle();
});

function insertChat(e) {
 var id = e.target.id;
 var notEmpty = $(".usertext").val() != "",
  isEnterKeypress = e.type == "keypress" && e.keyCode == 13,
  isSendClick = e.type == "click" && id == "send";

 if(notEmpty && (isEnterKeypress || isSendClick)) {
  var text = $(".usertext").val();
  var reply = $(".usertext").val();

  $('<p>' + text + '</p>').addClass("msg-send").appendTo(".msg-insert");
  $('.usertext').val(null);

  $('<p>' + reply + '</p>').addClass("msg-receive").appendTo(".msg-insert")
  $('.chat-body').scrollTop($(".chat-body").height());
  $(".usertext").val().clear();
 }
}
$(".chat-text").keypress(insertChat);

// END

// Calculator
var print = document.getElementById('print');
var erase = false;

Math.radians = function(degrees) {
  return degrees * Math.PI / 180;
};

Math.degrees = function(radians) {
  return radians * 180 / Math.PI;
};


var go = function(x) {
    if (x === 'ac') {
        print.value = null;
        erase = false;
    } else if (x === 'eval') {
        print.value = Math.round(evil(print.value) * 10000) / 10000;
        erase = true;
    } else if (x === 'sin' || x === 'tan') {
        print.value = Math.round(evil('Math.'+x+'(Math.radians('+evil(print.value)+'))') * 10000) / 10000;
        erase = true;
    } else if (x === 'log') {
        print.value = Math.round((Math.log(evil(print.value)) * 10000) / 10000);
        erase = true;
    } else if (erase === true) {
        print.value = x;
        erase = false;
    } else {
        print.value += x;
    }
};

function evil(fn) {
    return new Function('return ' + fn)();
}
// END

// GANTI TEMA

function changeTheme(theme) {
  console.log('theme', theme)
  $('body').css('background-color',theme.bcgColor);
}

localStorage.setItem("themes", JSON.stringify([
    {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
])
);

var daftarTheme = {
  0 : {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
  1 : {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
  2 : {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
  3 : {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
  4 : {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
  5 : {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
  6 : {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
  7 : {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
  8 : {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
  9 : {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
  10 : {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"},
}

var themeNow = {"Indigo": {"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};

if (!localStorage.getItem("selectedTheme")) {
  localStorage.setItem("selectedTheme", JSON.stringify(themeNow['Indigo']))
}

changeTheme(JSON.parse(localStorage.getItem("selectedTheme")))

$(document).ready(function() {
    $('.my-select').select2({
      'data': JSON.parse(localStorage.getItem("themes"))
    });
});

$('.apply-button').on('click', function() {
    var id = $('.my-select').val()
    var objTheme = daftarTheme[id];
    changeTheme(objTheme);
    localStorage.setItem("selectedTheme", JSON.stringify(objTheme));
})
