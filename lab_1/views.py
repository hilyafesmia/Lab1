from django.shortcuts import render
from datetime import date

# Enter your name here
mhs_name = 'Hilya Auli Fesmia' # TODO Implement this
birth_date = date(1999, 5, 23)

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age' : calculate_age(birth_date.year)}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
    today = date.today()
    return today.year - birth_year
