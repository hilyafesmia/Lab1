from django.conf.urls import url
from lab_6.views import index

urlpatterns = [
    url(r'^$', index, name='index'),
]
