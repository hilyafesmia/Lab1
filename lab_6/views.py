from django.http import HttpResponseRedirect
from django.shortcuts import render

# Create your views here.
mhs_name = 'Hilya Auli Fesmia'

def index(request):
    response = {'author': mhs_name}
    return render(request, 'lab_6/lab_6.html', response)
